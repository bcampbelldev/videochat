<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::post('online/clientSDP', 'online@client_SDP');
//Route::post('online/clientEndCall', 'online@client_EndCall');
//Route::post('online/clientAnswer', 'online@client_Answer');
//Route::post('online/clientCandidate', 'online@client_Candidate');

Route::get('user/currentUserId', 'userController@currentUserId');

Route::post('user/add_userPeerID', 'userController@add_userPeerID');
