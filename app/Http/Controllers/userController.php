<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class userController extends Controller
{
    //

    public function currentUserId(){
        $user = Auth::user();
        return $user->id;
    }

    public function add_userPeerID(Request $request){
        $user = Auth::user();
        $user->peerid = $request->peerid;
        $user->save();
    }
}
