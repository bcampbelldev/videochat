
let state = {
    showVideo : false,
    user: '',
    peerid: '',
    selfview: '',
    remoteview: '',
    users: [],
    call: null,
};

let getters = {
    showVideo : state => {
        return state.showVideo;
    },
    user: state => {
        return state.user;
    },
    peerid: state => {
        return state.peerid;
    },
    selfview: state => {
        return state.selfview;
    },
    remoteview: state => {
        return state.remoteview;
    },
    users: state => {
        return state.users;
    },
    call: state => {
        return state.call;
    }
};

let mutations = {
    set_showVideo : (state, newInput) => {
        state.showVideo = newInput;
    },
    add_user: (state, newInput) => {
        state.users.push(newInput);
    },

    set_peerid: (state, newInput)=>{
        state.peerid = newInput;
    },

    set_selfview: (state, newInput) => {
      state.selfview = newInput;
    },

    set_remoteview: (state, newInput) => {
        state.remoteview = newInput;
    },

    set_users: (state, newInput)=>{
        state.users = newInput;
    },

    set_user_peerid: (state, newInput) => {
        state.users.forEach(function(u, index, object){
            if(u.id == newInput.userid){
                u.peerID = newInput.peerID;
            }
        });
    },

    delete_user: (state, user) => {
        state.users.forEach(function(u, index, object){
           if(u.id == user.id){
               object.splice(index, 1);
           }
        });
    },

    update_call: (state, call) => {
        state.call = call;
    },

    remove_call: (state) => {
        state.call = null;
    }
};

let actions = {
    update_showVideo: (context, payload) => {
        context.commit('set_showVideo', payload);
    },
    update_user: (context, payload) => {
        context.commit('set_user', payload);
    },

    add_user: (context, payload) => {
      context.commit('add_user', payload);
    },

    update_peerid: (context, payload) => {
        context.commit('set_peerid', payload);
    },

    update_users: (context, payload) => {
        context.commit('set_users', payload);
    },

    remove_user: (context, payload) => {
        context.commit('delete_user', payload);
    },

    update_user_peerid: (context, payload) => {
        context.commit('set_user_peerid', payload);
    },

    call_user: (context, payload) => {
        var constraints = {audio: true, video: true}
        window.getUserMedia(constraints, function(stream) {
            context.commit('set_selfview', stream);

            var call = window.peer.call( payload.calling, stream);


            call.on('stream', function(remoteStream) {
                // Show stream in some video/canvas element.
                context.commit('set_remoteview', remoteStream);
            }).on('close', function(){
                context.commit('set_remoteview', '');
                context.commit('set_selfview', '');
            });

            context.commit('update_call', call);

        }, function(err) {
            console.log('Failed to get local stream' ,err);
        });

    },

    set_call: (context, payload) => {
        context.commit('update_call', payload);
    },

    answer_call: (context, payload) => {
          var answer = confirm(
              "You have a call from: " + ' ' + " | Would you like to answer?"
          );
          if (!answer) {

          }

        var constraints = {audio: true, video: true}
        window.getUserMedia(constraints, function(stream) {
            context.commit('set_selfview', stream);

            payload.answer(stream); // Answer the call with an A/V stream.
            payload.on('stream', function(remoteStream) {
                // Show stream in some video/canvas element.
                context.commit('set_remoteview', remoteStream);
            });
        }, function(err) {
            console.log('Failed to get local stream' ,err);
        });
    },

    remove_selfview: (context, payload) => {
      context.commit('set_selfview', '');
    },

    remove_remoteview: (context, payload) => {
        context.commit('set_remoteview', '');
    },

};


export default {
    state,getters,mutations,actions
}
