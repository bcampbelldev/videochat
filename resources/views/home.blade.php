@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <selfview></selfview>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="card">
                <div class="card-header">Online Users</div>

                <div class="card-body">
                    <userlist :currentUserId={{ Auth::user()->id }}></userlist>

                </div>
            </div>
        </div>
    </div>



</div>
@endsection
